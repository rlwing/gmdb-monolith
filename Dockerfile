FROM openjdk:11-jdk as build
VOLUME /tmp
COPY . .
RUN ./gradlew clean bootJar

FROM openjdk:11-jdk
WORKDIR /app
COPY --from=build build/libs/*.jar app.jar
ARG JAR_FILE
ENTRYPOINT ["java", "-jar", "app.jar"]
EXPOSE 8080

#docker build -t gmdb/monolith .
#docker run -d -p8080:8080 \
#	--name monolith \
#	--network gmdb-bridge \
#	-eDB_HOST=monodb \
#	gmdb/monolith
